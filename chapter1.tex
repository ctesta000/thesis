% This should include motivation, description of the paper, and assumed background material

\chapter{Introduction}

% In this paper I will discuss elliptic curves, some of the most common vernacular associated with them, and explain some of the popular theorems and conjectures surrounding them.  I will begin with a chapter that introduces the reader to a litany of vocabulary and ideas that are necessary to understand the theorems I have included.  In this first chapter many objects associated with elliptic curves will be constructed and defined.  Next, an introduction to SageMath will be provided.  In the subsequent chapter I will create the backdrop for a major area of interest through computational exploration and by use of more refined theorems.  It is in this chapter that I would like to include some of the most compelling mathematics and data visualization to make tangible some of the mystery surrounding elliptic curves.  Lastly, in my conclusion I will provide further directions in the way of elliptic curves that take advantage of modern computational capability and explorative
% programming.

In this thesis I will explain elliptic curves, some common vocabulary surrounding them,
some popular relevant theorems and conjectures, and perform an investigation into a subject
of my interest. I will begin with a short introductory chapter to the necessary vocabulary
and ideas to formally state what an elliptic curve is so that the reader may
always have solid grounding as we tour through elliptic curves'
history in the second chapter. The third chapter of this thesis is a single page
cheat-sheet
to the parts of SageMath which I have found the most relevant to elliptic curves
through my studies. Last, the fourth chapter is an investigation
into how the infinite part of rational elliptic curves and the structure
of finite field elliptic curves are interrelated. In writing this thesis, it is
my hope that I can make tangible some of the mystery surrounding elliptic curves
through data visualization and historical exposition. Finally, I would like
to inspire the notion that even through explorative programming and
heuristic arguments we may create new scientific discoveries in the world of mathematics.

Prerequisite knowledge assumed includes some familiarity with
fields, polynomials, sets, calculus, and cyclic group theory.
The integers $\mathbb Z$,
rational numbers $\mathbb Q$,
real numbers $\mathbb R$,
complex numbers $\mathbb C$,
finite fields $\mathbb F_q$,
and an arbitrary field $k$ will make
frequent appearances.

All original pictures which appear in this thesis
are hosted online along with example code. Professor Drew Sutherland at MIT has
kindly allowed me to use the Sato-Tate Distribution image from his website.
The Wikimedia Commons is the source for another three images in this
thesis, which appear in the Weierstra{\ss} Elliptic $\wp$ Function figure. 

\section{What is an Elliptic Curve?}

Before we can describe an elliptic curve, we must know something about curves.
Curves exist in lots of different contexts: in the cartesian plane, Euclidean space of
arbitrary dimension over arbitrary fields, and a whole lot more.  Further, for every field $k$,
there are many different ways we could construct space with coordinates in $k$
in which a curve could exist.  We will consider two spaces that exist
for any field $k$, the affine and projective spaces, before we consider
the elliptic curves which exist within them.

% Most individuals are familiar with some curves already,
% like lines, conic sections, or even some famous examples
% like the Fermat Curve $x^n + y^n = z^n$.  The key unifying feature of
% these objects, and indeed including elliptic curves, is that they form
% one dimensional solution spaces.

%     - Affine Plane
The \textbf{affine plane over $k$}, denoted $\mathbb A^2(k)$, is the set
of points in $k \times k$ for a given field $k$.
A familiar example of affine space is Euclidean 2 or 3 dimensional space,
which are denoted here $\mathbb A^3(\mathbb R)$ and $\mathbb A^3(\mathbb R)$.
This generalizes to affine space over $k$, $\mathbb A^n(k)$,
the set of $n$-tuples in $k$.

%     - Projective Plane
The \textbf{projective plane} over $k$ is given by the set of tuples
\[ \mathbb P^2 (k) = \{ (x,y,z) \in k^3 | (x,y,z) \neq (0,0,0) \} / \sim, \]
where $\sim$ denotes the equivalence relation where
$(x,y,z) \sim (x',y',z')$ if and only if there exists some scaling
factor $c \neq 0$ such that $(x',y',z') = (cx,cy,cz)$.

% This generalizes similarly to projective space in $n$ dimensions as
% \[ \mathbb P^n(k) = \{ (x_1,x_2,...,x_n) \in k^n | (x_1,x_2,...,x_n) \neq (0,0,...,0) \} / \sim.  \]

This equivalence relation can be intuitively thought of as stating
that two coordinates are considered the same point in projective $2$-d
space if and only if the same coordinates in affine $3$-d space lie on the same
line through the origin.

A point on the projective plane is traditionally denoted $(x : y : z)$ since it is
only the ratios between among the coordinates that determines the point.  That is,
the ratios $x:y$, $y:z$, and $z:x$ will remain constant regardless of the coordinate
representation of the point in $\mathbb P^2 (k)$.

An algebraic \textbf{affine plane curve} is a nonempty set of points in $\mathbb A^2 ( k )$
which are the solutions of some polynomial in two variables with coefficients in $k$. That is, if $f$ is a polynomial equation $f(x,y) \colon \mathbb A^2(k) \to k$ then a nonempty set of points $\{ (x,y) | f(x,y) = 0 \}$ is
an algebraic affine plane curve.

An algebraic \textbf{projective plane curve} is a nonempty set of points in $\mathbb P^2(k)$
which are the solutions of some homogeneous polynomial in the projective
plane $f(x:y:z) \colon \mathbb P^2(k) \to k$ with coefficients in $k$.

% We define the \textbf{degree of a curve} to be the greatest sum of the
% exponents of the variables of any single term.  If a curve has only constant
% and variable terms with exponent one then it is linear.
% If a curve has at most $x^2$, $y^2$, $xy$ or other exponent 2 terms then it
% is quadratic.  We say if a curve has at most degree three terms, it
% is cubic, at most degree four terms, then it is quartic, and a curve with at
% most exponent five terms is quintic.
%
The \textbf{degree} of an algebraic plane curve is taken to be the
greatest sum of exponents on $x$ and $y$ in any single term of
the polynomial for which the points on the curve are the
solution set.
A polynomial is \textbf{homogeneous} if every term of the polynomial
has the same degree.  This is a necessary condition a polynomial
whose solution set is an algebraic curve in the
projective plane must satisfy
in order to be well defined with respect to the equivalence
relation used to construct the projective plane.
% Examples include $x+y=0$, or $x^2 + y^2 + xy = 0$.  This becomes
% a necessary condition to define a projective plane curve given its equation
% $f$.  If two points in projective space are equal, then their valuation under
% $f$ must be equal in order to meaningfully state whether or not they lie on
% the curve.  This imposes the condition that a projective curve must be homogeneous.
\begin{figure}[H]
  \centering
  \begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.7\textwidth]{ch2-singular-node}
  \caption*{Crunode: $y^2=x^3+x^2$}
  \centering
  \end{subfigure}%
  \begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.7\textwidth]{ch2-singular-cusp}
  \caption*{Ordinary Cusp: $y^2=x^3$}
  \end{subfigure}
  \begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.7\textwidth]{ch2-acnode}
  \caption*{Acnode: $y^2=x^3-x^2$}

  \end{subfigure}
  \caption{Different Kinds of Degree Three Singularities.}
\end{figure}

We say that a point on a curve is \textbf{singular}
if every partial derivative vanishes.  That is,
if $f$ is a curve in $n$ dimensional space, either affine or projective,
then we say that $f$ is singular at some point if
\[ \frac{\partial f}{\partial x_1} = \frac{\partial f}{\partial x_2} = ...
= \frac{\partial f}{\partial x_n} = f(x_1, x_2, ..., x_n) = 0.\]
A curve is \textbf{singular} if it contains one or more singular
points.  Naturally, we say a curve is \textbf{nonsingular} if it contains no
singular points.

% \textbf{Example.} A conic section is a nonsingular degree two plane curve.
% The parabolas, hyperbolas, and ellipses are all degree two polynomials.
% % really I should have an example of each of these:
% - different degree curves
% - some singular curves
% - some projective vs affine curves

Singularities come in two kinds: nodes and cusps.  Each of
these break down into further subcategories.  For double points,
the possibilities include acnodes, crunodes, and ordinary cusps,
while singularities of higher multiplicity could be tacnodes and rhamphoid cusps.
We will only be concerned with the former in the context of elliptic curves.

% I'd really love to include the fact that cusps actually show up in nature
% all the time!
% https://en.wikipedia.org/wiki/File:Caustic00.jpg

Finally, we can make the following definition.
\begin{definition*}
An \textbf{elliptic curve}
is a degree three nonsingular plane curve, affine or projective,
with the point at infinity denoted $\mathcal O$.
\end{definition*}

In the affine case, the point at infinity $\mathcal O$ can be thought of
as a symbol we pair the
curve with, and the point $\mathcal O$ itself does not exist in the $(x,y)$
plane over $k$.  In the projective case, the point at infinity is simply
the point where the curve intersects the line at $z=0$. We will need
this point at infinity as the identity to realize the group structure of
elliptic curves.
\vspace{.25in}
\begin{figure}[H]
  \centering
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/1}
  \caption*{$y^2=x^3+x+1$}

  \end{subfigure}%
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/2}
  \caption*{$y^2 = x^3 - x + 1$}

  \end{subfigure}%
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/3}
  \caption*{$y^2 = x^3 - x$}

  \end{subfigure}%
  \caption{Three Example Elliptic Curves}
  \label{fig:ch2-first-curves}

\end{figure}
