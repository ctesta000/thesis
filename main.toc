\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\nag@@warning@xxii \relax \fontsize {10}{12}\selectfont \abovedisplayskip 10\p@ plus2\p@ minus5\p@ \abovedisplayshortskip \z@ plus3\p@ \belowdisplayshortskip 6\p@ plus3\p@ minus3\p@ \def \leftmargin \leftmargini \parsep 5\p@ plus2.5\p@ minus\p@ \topsep 10\p@ plus4\p@ minus6\p@ \itemsep 5\p@ plus2.5\p@ minus\p@ \topsep \z@ \parsep \parskip \itemsep \z@ {\leftmargin \leftmargini \topsep 6\p@ plus2\p@ minus2\p@ \parsep 3\p@ plus2\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \add@extra@listi {ftns}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}What is an Elliptic Curve?}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}The History of Elliptic Curves}{4}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Discovery and Early Ideas}{5}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Point Doubling}{6}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Newton's Consideration of Curves}{6}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Bezout's Theorem}{7}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Addition and Multiplication of Points}{8}{subsection.2.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}From Integrals to $E / \mathbb C$}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Elliptic Integrals}{9}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Elliptic Functions}{11}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Weierstra{\ss }' $\wp $}{11}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.4}$E / \mathbb C$ as a Torus}{13}{subsection.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Rational Elliptic Curves $E / \mathbb Q$}{14}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Fermat's Descent}{15}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Mordell-Weil Theorem}{16}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Nagell-Lutz Theorem}{18}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Mazur's Theorem}{18}{subsection.2.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Elliptic Curves over Finite Fields $E / \mathbb F_q$}{18}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Hasse's Bound}{19}{subsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Sato-Tate Theorem}{20}{subsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}The Birch and Swinnerton-Dyer Conjecture}{21}{subsection.2.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.4}Lenstra's Factorization Algorithm}{21}{subsection.2.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.5}Elliptic Curve Cryptography}{22}{subsection.2.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Modular Forms}{23}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Modularity Theorem}{24}{subsection.2.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}MacDonald's Equation}{24}{subsection.2.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}On the Present}{25}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Introduction to Elliptic Curves in SageMath}{26}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}A Numerical Investigation}{27}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Reducing $E(\mathbb {Q})$ into $E(\mathbb {F}_p)$}{27}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Generating a Subgroup with $(0,0)$}{34}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}When is the Subgroup the Group?}{39}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}On Progress}{42}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{43}{chapter*.39}
