\chapter{Background and Major Theorems}

This chapter is about the arithmetic of elliptic curves. As in,
how does one calculate features both about elliptic curves and
through using elliptic curves. This discussion will be broken by
the context of the base field. However, before talking about any
specific context we will discuss how point addition on an elliptic
curve is constructed as it can be analogously constructed for an
arbitrary field. After discussing point addition, we will
dive into the structure of elliptic curves over the rational numbers,
finite fields, and complex numbers. In all of these contexts, startling
structure appears in the arithmetic of elliptic curves.

% E(Q):
%   - Point Addition
%   - Mordell's Theorem
%   - Mazur's Theorem
%   - Nagell-Lutz Theorem
%   - Thue's Theorem
%   - Taxicab Numbers and the K3 Surface
%   - Rank Distributions Conjectures

% E(F_p)
%   - Elliptic Curve Cryptography
%   - Riemann Hypothesis for Curves
%   - BSD
%   - Hasse Bound
%   - Sato-Tate Theorem


% E(C)
%   - The Weierstrauss $\wp$ Function
%   - Complex Lattice Isomorphisms
%         -> Torus

% How do I fit these in?
%   - Modularity Theorem

\section{Point Addition}
We will consider points on elliptic curves for a moment.

Let $F$ be the nonconstant homogeneous polynomial $F \in k[x,y,z]$ that defines our
elliptic curve $E$. The set of $\mathbb P^2 (k)$ points of an elliptic curve is denoted
\[E(k) = \{ (x:y:z) \in \mathbb P^2 \text{ : } F(x,y,z) = 0 \}.
\]

Often when writing a point down for an elliptic curve, $(x,y)$ is used to denote $(x:y:1)$ since
it is understood that any projective point with $z \neq 0$ can be transformed to a point of this form.
This says that except for the point $(0:1:0) = \mathcal O$, all other points of
an elliptic curve can be coerced into the form $(x,y:1)$. We will use this for its combination
of notation and graphical convenience.

\textbf{Bezout's Theorem} states that two projective plane curves $C$ and $D$ over a field $k$ with degrees
$m$ and $n$ respectively having no irreducible common factors intersect in the algebraic closure $k^{\text{al}}$
exactly $mn$ times counting multiplicity.

With Bezout's Theorem in mind, we construct an addition formula where three collinear points
sum to the identity.

The intersection of a vertical line and an elliptic curve to form either a triple root at infinity,
a double root with a single root at infinity, or two distinct roots in the affine $\mathbb Q \times \mathbb Q$
plane and a single root at infinity. Using this fact about elliptic curves, we define geometrically the
addition of two $E (\mathbb Q)$ points.

Given $P$ and $Q$ as two points in $E(\mathbb Q)$, there are three possibilities. $P$ and $Q$ could be the same point,
in which case we construct $\overline{PQ}$ as the tangent line to the elliptic curve at $P$. $P$ and $Q$ could have
the same $x$-coordinate, in which case $\overline{PQ}$ is a vertical line. Or $P$ and $Q$ are two distinct points
with different $x$-coordinates such that the line passing through them $\overline{PQ}$ has a finite slope.

In all cases, we find the third point of intersection between $E$ and $\overline{PQ}$ and label it $PQ$.
$PQ$ is an intermediary point we will to construct the vertical line intersecting
$\mathcal O$ and $PQ$. This vertical line, $\overline{O(PQ)}$ intersects $E$ in three places counting multiplicity,
and so we take the remaining root to be $P+Q$.

\begin{figure}[H]
\centering
\includegraphics[width=0.90\textwidth]{ch2-point-addition}
\caption{Point Addition}

\label{fig:ch3-point-addition}
\end{figure}

To make this geometric construction more formal and algorithmic, we use the following addition
and duplication formulas. In the following formulas, we will assume that $k$ does not have
characteristic 2 or 3, and therefore any elliptic curve can be assumed to be of the form
\[ E : y^2 z = x^3 + ax z^2 + bz^3, \\ a,b \in K \\ \Delta = 4a^3 + 27b^2 \neq 0. \]

Suppose we have two points $P$ and $Q$ on an elliptic curve and we want to add them.
If $P$ and $Q$ are above one another, we construct the vertical line and see that
intersects the point at infinity. This is the condition that if $P = -Q$, then
$P+Q = \mathcal O$.

This is how we compute the additive inverse of a point $P$, $-P$., we
know by Bezout's theorem there are three points counting multiplicity
which lie on the vertical line intersecting an elliptic curve. Based on
how we've constructed elliptic curve point addition, the point which
satisfies $P + -P = \mathcal O$ is the third intersection point. This
third intersection may very well be $P$ in some cases, in which case $P$
is an order two point. That is, $2P = \mathcal O$. This suggests the
notion of doubling and multiplying points on an elliptic curve.

\begin{figure}[H]
\centering
\includegraphics[width=0.90\textwidth]{ch2-point-multiplication}
\caption{Point Multiplication}

\label{fig:ch3-point-multiplication}
\end{figure}

We have talked about adding two points, but we can also multiply a given point.
To double the point, we take the tangent line at the point $P$, find its next
intersection with $E$, and then find the corresponding other vertical intersection.
If $P = Q$ we use the following \textbf{duplication formula}:

Let $P = (x,y)$ and $2P = (x_2,y_2)$. If $y=0$, then $2P = 0$. Otherwise, we define:
\[ x_2 = \frac{(3x^2 + a) - 8xy^2}{4y^2} = \frac{x^4 - 2ax^2 - 8bx + a^2}{4(x^3 + ax + b)}
\]
\[ y_2 = \frac{x^6 + 5ax^4 + 20bx^3 - 5a^2x^2 - 4abx - a^3 - 8b^2}{(2y)^3}.
\]

However, most frequently we will simply be using the point addition formula as follows.
If $P = (x_1,y_1)$ and $Q = (x_2,y_2)$ are two points with $x_1 \neq x_2$ we use the
\textbf{addition formula} to
construct the point $P + Q = (x,y) \in E(k)$. We determine $(x,y)$ using the following formulas:
\[ x(x_1 - x_2)^2 = x_1 x_2^2 + x_1^2x_2 - 2y_1 y_2 + a(x_1 + x_2) + 2b
\] and
\[ y(x_1 - x_2)^3 = W_2y_2 - W_1y_1
\] where
\[ W_1 = 3x_1x_2^2 + x_2^3 + a(x_1 + 3x_2) + 4b \\
W_2 = 3x_1^2x_2 + x_1^3 + a(3x_1 + x_2) + 4b.
\]

With Sage, we often need not worry about the details of the arithmetic ourselves.

\begin{sage}
#   Let's get an example curve.
    E = EllipticCurve('389a1')

#   Points can be declared two ways
    P = E.point(((4, 8))); P
''' (4 : 8 : 1)
    Q = E((1,0)); Q
''' (1 : 0 : 1)
'''
#   Addition and subtraction couldn't be more natural.
    P + Q
''' (10/9 : -35/27 : 1)
''' P - Q
''' (3 : -6 : 1)

#   Multiplying points is no problem!
    2*P
''' (315/289 : 1197/4913 : 1)
''' 4*P
''' (48715689378/15430359961 : -12358066605905659/1916743883995459 : 1)
\end{sage}

\section{$E(\mathbb Q)$}

Elliptic curves over the rational numbers is a rich subject contained in
the intersection number theory, algebraic geometry, and Diophantine equations.
Significant progress has been made on understanding integer points and points
of finite order, however thoroughly understanding elliptic curve's infinite structure
remains difficult.

\begin{wrapfigure}{l}{0.35\textwidth}
   \centering
   \includegraphics[width=0.35\textwidth]{ch2-generated-points/1129211a1-bold.png}
   \caption{Small Sums of Generators}
   \label{fig:ch3-generating-points}
\end{wrapfigure}

\textbf{Mordell's Theorem} proven in 1922 states that $E(\mathbb Q)$ is a finitely generated
abelian group.

\textbf{Mazur's Theorem} states that an elliptic curve's torsion part is isomorphic to
one of the following:
\begin{itemize}
  \item $\mathbb Z/n\mathbb Z \text{  for } 1 \leq n \leq 10 \text{ or } n = 12$
  \item $\mathbb Z/2\mathbb Z \times \mathbb Z/n\mathbb Z \text{ for } n = 2,4,6,9$.
\end{itemize}
Further, each of these torsion structures occur for infinitely many
elliptic curves over $\mathbb Q$. Mazur's theorem was proven in 1977.

\textbf{Falting's Theorem}

%   - Mordell's Theorem 1922-23
%     https://www.win.tue.nl/~aeb/2WF02/mordell.pdf
%     https://en.wikipedia.org/wiki/Mordell%E2%80%93Weil_theorem
%
%   - Mazur's Theorem
%     http://www.math.harvard.edu/theses/senior/schwartz/thesis.pdf
%
%   - Nagell-Lutz Theorem
%   - Thue's Theorem
%   - Taxicab Numbers and the K3 Surface
%   - Rank Distributions Conjectures
