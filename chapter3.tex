\chapter{Introduction to Elliptic Curves in SageMath}

I would suggest that anybody interested in computational
mathematics try out SageMath. The proximity of the language
to Python makes it easy to pick up, and there are wonderful
libraries like numpy, pandas, and matplotlib which make working with
data in SageMath a delight. To get help in Sage, I use ?, help, dir, and the
inspect module's getsource function. For example, {\scriptsize\texttt{EllipticCurve?}} will return
Sage documentation pages for the elliptic curve constructor.
Help returns Python object documentation, dir
returns a Python object's list of methods, and getsource let's you take
a look at a Python method's source code.

\begin{multicols}{2}
\begin{sage}
#   We can use the EllipticCurve constructor in a
#   variety of ways. Using LMFDB or Cremona's labels,
#   through $a$-invariants (long or short), polynomially,
#   or by the $j$-invariant are all options.
    E = EllipticCurve('496a1')
    E = EllipticCurve([0, 0, 0, 1, 1])
    E = EllipticCurve([1,1])
    var('x y'); E = EllipticCurve(y^2 == x^3 + x + 1)
    E = EllipticCurve_from_j(6912/31)
''' Elliptic Curve defined by y^2 = x^3 - x
    over Rational Field
'''
  # Point arithmetic works great!
    E(1,0) + E(2,2)
''' (1 : -1 : 1)
''' 2*E(2,2)
''' (21/25 : -56/125 : 1)
'''
   #Any commutative ring may be used to construct
   #elliptic curves.
    R.<a> = PolynomialRing(QQ, 'a')
    EllipticCurve(R, [0,0,1,a,0])
''' Elliptic Curve defined by y^2 + y = x^3 + a*x over
    Univariate Polynomial Ring in a over Rational Field

''' E = EllipticCurve(FiniteField(3), [1,1])
''' Elliptic Curve defined by y^2 = x^3 + x + 1
    over Finite Field of size 3

''' E.change_ring(QQ)
''' Elliptic Curve defined by y^2 = x^3 + x + 1 over
    Rational Field

''' EllipticCurve(pAdicField(p=5, prec=20), [1,-1])
''' Elliptic Curve defined by y^2 = x^3 +
    (1+O(5^20))*x + (1+O(5^20)) over 5-adic Field
    with capped relative precision 20

''' EllipticCurve(QQ, [1,1]).reduction(3)
''' Elliptic Curve defined by y^2 = x^3 + x + 1 over
    Finite Field of size 3
'''
  # Plotting $E$ over $\R$, $\Q$, and $\F_p$ is no problem.
    E.plot()

  # We can easily compute many features of any $E / \Q$
    E.ainvs()           # $a$-invariants
    E.analytic_rank()   # $\text{ord}_{s=1} L(E,s)$
    E.conductor()       # conductor of $E$
    E.discriminant()    # discriminant $\Delta$
    E.gens()            # free generators of $E(\Q)$
    E.has_cm()          # check for complex multiplication
    E.j_invariant()     # $j$-invariant
    E.lseries()         # $L(E,s)$
    E.Np(p)             # $|E(\F_p)|$ for prime argument
    E.rank()            # rank of $E$
    E.torsion_points()  # finite order points
    E.integral_points() # integer coordinate points

  # We can get specifically structured curves from
  # sage.schemes.elliptic_curves.ec_database
    elliptic_curves.rank(n=3, rank=3, tors=2, labels=True)
''' ['59450i1', '59450i2', '61376c1']
'''
  # Using the @parallel decorator, we convert a normal
  # function into one which runs on lists of input in
  # parallel. The function returns a generator,
  # which when iterated on returns the values as they
  # are computed in parallel, with their arguments
  # and results listed.
    @parallel
    def parallel_Np(E,p): return E.Np(p)
    l = zip(elliptic_curves.rank(0), primes_first_n(10))
    for answer in parallel_Np(l): print answer
''' (((Elliptic Curve defined by y^2 + y = x^3 - x^2 -
    7820*x -263580 over Rational Field, 3), {}), 5)
    (((Elliptic Curve defined by y^2 + y = x^3 - x^2 -
    10*x - 20 over Rational Field, 2), {}), 5)
    (((Elliptic Curve defined by y^2 + y = x^3 - x^2
    over Rational Field, 5), {}), 5)
    ...
'''
\end{sage}

\end{multicols}
