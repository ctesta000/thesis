\chapter{A Quickstart Guide}

% Chapter Layout:
% -   Definitions

%     - Affine Plane
%     - Affine Curves
%     - Projective Plane
%     - Degree
%     - Homogeneity
%     - Projective Curves
%     - An Elliptic Curve
%       - Affine Definitions
%       - Projective
%       - Complex
%       - Genus
%     - Weierstrass Form
%     - $j$-Invariant
%     - Elliptic Curve Databases
%     - Conductor

\begin{figure}[H]
  \centering
  Three Example Elliptic Curves\vspace{.25in}
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/1}
  \caption{$y^2=x^3+x+1$}

  \end{subfigure}%
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/2}
  \caption{$y^2 = x^3 - x + 1$}

  \end{subfigure}%
  \begin{subfigure}{.3\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{ch2-first-curves/3}
  \caption{$y^2 = x^3 - x$}

  \end{subfigure}%
  \label{fig:ch2-first-curves}

\end{figure}


This chapter begins with introducing the terminology necessary to construct an elliptic curve.
After introducing the abstract definition of an elliptic curve, the Weierstrass form, $j$-invariant,
discriminant, and elliptic curve database labels will be introduced to lend context to the subject.
Elliptic curves are a fascinating subject, with many theorems each worth their own investigation,
but before doing such investigation we must have a solid understanding of how to build an elliptic curve.

\section{Defining an Elliptic Curve}

Before we can describe an elliptic curve, we must know something about ``curves."
Curves live in lots of different contexts: on surfaces, in the cartesian plane, Euclidean space of
arbitrary dimensions, and a whole lot more. Further, for every base field $k$,
there are many different ways we could construct space with coordinates in $k$
in which a curve could exist. We will consider two specific spaces that exist
for any arbitrary field $k$: the affine and projective spaces, before we consider
the elliptic curves which exist within them.

Most individuals are familiar with some curves already,
like lines, conic sections, or even some famous examples
like the Fermat Curve $x^n + y^n = z^n$. The key unifying feature of
these objects, and indeed including elliptic curves, is that they form
one dimensional solution spaces.

%     - Affine Plane
The \textbf{affine plane over $k$}, denoted $\mathbb A^2(k)$, is the set
of points in $k \times k$ for a given field $k$.
A familiar example of affine space is Euclidean 2 or 3 dimensional space,
which is denoted here $\mathbb A^3(\mathbb R)$.
This generalizes to affine space over $k$, $\mathbb A^n(k)$,
the set of $n$-tuples in $k$.

An \textbf{affine plane curve} is the set of points in $\mathbb A^2 ( k )$
given by the zeroes of some bivariate polynomial with coefficients in $k$.
That is, it is the solutions of some $f(x,y) = 0$ polynomial equation.

%     - Projective Plane
The \textbf{projective plane} is given by the set of tuples
\[ \mathbb P^2 (k) = \{ (x,y,z) \in k^3 | (x,y,z) \neq (0,0,0) \} / \sim, \]
where $\sim$ denotes the equivalence relation where
$(x,y,z) \sim (x',y',z')$ if and only if there exists some scaling
factor $c$ such that $(x',y',z') = (cx,cy,cz)$.

This generalizes similarly to projective space in $n$ dimensions as
\[ \mathbb P^n(k) = \{ (x_1,x_2,...,x_n) \in k^n | (x_1,x_2,...,x_n) \neq (0,0,...,0) \} / \sim. \]

An equivalent way to state the definition for this equivalence relation is
that two coordinates are considered the same point in projective $n$-dimensional
space if and only if the coordinates in affine $n+1$ dimensional space lie on the same
line through the origin.

A point on the projective plane is traditionally denoted $(x : y : z)$ since it is
only the ratios between among the coordinates that determines the point. That is,
the ratios $x:y$, $y:z$, and $z:x$ will remain constant regardless of the coordinate
representation of the point in $\mathbb P^2 (k)$.

We define the \textbf{degree of a curve} to be the greatest sum of the
exponents of the variables of any single term. If a curve has only constant
and variable terms with exponent one then it is linear.
If a curve has at most $x^2$, $y^2$, $xy$ or other exponent 2 terms then it
is quadratic. We say if a curve has at most degree three terms, it
is cubic, at most degree four terms, then it is quartic, and a curve with at
most exponent five terms is quintic.

Another condition that a polynomial equation can satisfy is that of
homogeneity. An equation is \textbf{homogeneous} if every term has the same
degree. Some examples could be $x+y=0$, or $x^2 + y^2 + xy = 0$. This becomes
a necessary condition to define a projective plane curve given its equation
$f$. If two points in projective space are equal, then their valuation under
$f$ must be equal in order to meaningfully state whether or not they lie on
the curve. This imposes the condition that a projective curve must be homogeneous.

A \textbf{projective plane curve} is defined to be the set of
$\mathbb P^2(k)$ solution points to a homogeneous polynomial $f(x,y,z)$ with
coefficients in $k$ which contains more than one point. (If a homogeneous
equation polynomial $f$ has only zero or one projective point which satisfies it,
then its solution space is zero dimensional, and it is not considered to be a
``curve.")

We say that a point on a curve is \textbf{singular}
if every partial derivative vanishes. That is,
if $f$ is a curve in $n$ dimensional space, either affine or projective,
then we say that $f$ is singular at some point if
\[ \frac{\partial f}{\partial x_1} = \frac{\partial f}{\partial x_2} = ...
= \frac{\partial f}{\partial x_n} = f(x_1, x_2, ..., x_n) = 0.\]
We say that a curve is \textbf{singular} if it contains one or more singular
points. Naturally, we say a curve is \textbf{nonsingular} if it contains no
singular points.

\textbf{Example.} A conic section is a nonsingular degree two plane curve.
The parabolas, hyperbolas, and ellipses are all degree two polynomials.
% really I should have an example of each of these:
% - different degree curves
% - some singular curves
% - some projective vs affine curves

\textbf{Example.} Singularities come in two kinds: nodes and cusps. Each of
these break down into further subcategories. For double points,
the possibilities include acnodes, crunodes, and ordinary cusps,
while multiple points on higher degree curves can be tacnodes and rhamphoid.
We will only be concerned with the former in the context of elliptic curves.

% I'd really love to include the fact that cusps actually show up in nature
% all the time!
% https://en.wikipedia.org/wiki/File:Caustic00.jpg
\begin{figure}[H]
  \centering
  \begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{ch2-singular-cusp}
  \caption{Ordinary Cusp: $y^2=x^3$}

  \end{subfigure}%
  \begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{ch2-singular-node}
  \caption{Crunode: $y^2=x^3+x^2$}

  \end{subfigure}

  \begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{ch2-acnode}
  \caption{Acnode: $y^2=x^3-x^2$}

  \end{subfigure}
  \begin{subfigure}{.45\textwidth}
  \centering
  \includegraphics[width=0.45\textwidth]{ch2-tacnode}
  \caption{Tacnode: $(x^2 + y^2 - 3x)^2 = 4x^2(2-x)$}
  \end{subfigure}
  \label{fig:ch2-singularities}
  \caption{Different Kinds of Singularities.}
\end{figure}

Note that the first three are cubics, while the fourth is a quartic curve.

\textbf{Remark.} I highly recommend taking a look at Wikipedia's
\href{https://en.wikipedia.org/wiki/List_of_curves}{List of Curves}. Some
of my favorites pages linked there are the
\href{https://en.wikipedia.org/wiki/Klein_quartic}{Klein Quartic},
\href{https://en.wikipedia.org/wiki/Watt%27s_curve}{Watt's Curve},
and the
\href{https://en.wikipedia.org/wiki/Quartic_plane_curve#Bicuspid_curve}{Quartic
Plane Curve}.

% i really don't think I want to include this here.
% it's a strange first example.

% \textbf{Example.}
% $y^2z = x^3 - xz^2$ is a nonsingular projective degree 3 curve.
% Since it is homogeneous, it makes sense to talk about the lines passing
% through the origin that the equation contains. Each of them defines a
% point in projective space. After a single modification, this can be
% considered an elliptic curve.


%% I really want to include two different definitions here, one projective
% and one affine.
Finally, we can make the following definition: an \textbf{elliptic curve}
over $k$ is a cubic nonsingular plane curve, affine or projective,
with coordinates in $k$, with a rational point over $k$ denoted $\mathcal O$
and called the point at infinity.

In the affine case, the point at infinity $\mathcal O$ is a symbol we pair the
curve with, and the point $\mathcal O$ itself does not exist in the $(x,y)$
plane over $k$. In the projective case, the point at infinity is simply
the point where the curve intersects the line at $z=0$.

% The above definitions for affine and projective curves are equivalent definitions
% for an elliptic curve, since an affine curve can be uniquely converted to a projective curve
% and vice versa. Two further definitions describe elliptic curves over $\mathbb C$
% and through the genus of a curve.
%
% An \textbf{elliptic curve over $\mathbb C$} is isomorphic to $\mathbb C / \Lambda$
% for some lattice $\Lambda$. That is, \[
% \Lambda = \{ a \omega_1 + b\omega_2 \text{ : }
% a,b \in \mathbb Z, \/ \omega_1 \text{ and } \omega_2 \text{ are linearly independent}\},
% \] and our elliptic curve is isomorphic to the fundamental parallelogram
% constructed from $\omega_1$ and $\omega_2$.


% To go from a projective curve to an affine curve, we exclude the point
% at infinity on the curve, as in the intersection of the curve with $z=0$,
% and we scale every other point such that $z=1$. This yields the elliptic
% curve as the set of $(x,y) \in \mathbb A^2(k)$
% pairs such that $(x:y:1) \in \mathbb P^2(k)$ is on an elliptic curve $E$,
% and the point of infinity $\mathcal O$ is included implicitly.
%
% This should also
\textbf{Remark.} The images on the right would suggest correctly that one
can go back and forth between corresponding projective and affine curves.
To go from the projective form to the affine form, simply substitute
$z=1$. To go from an affine to projective equation, one uses the
process of ``homogenizing an equation." An additional variable is introduced,
namely $z$, and each term is multiplied by a power of $z$ such that every
term has the same degree. The result is the corresponding homogeneous
projective elliptic curve.

\begin{wrapfigure}{r}{0.25\textwidth}
   \centering
   \includegraphics[width=0.25\textwidth]{ch2-projective-curves/projective-curve}
   \includegraphics[width=0.25\textwidth]{ch2-projective-curves/affine-curve}

   \caption{}
   $y^2z = x^3 + xz^2 + z^3$
   and
   $y^2 = x^3 + x + 1$
   \label{fig:ch2-projective-11a}
\end{wrapfigure}

\textbf{Remark.} While projective space might be structurally different
than affine space, that doesn't stop us from taking a look at the
embedding of the projective plane and a projective curve into
affine space. Of course, since there is a sense in which a point
in projective space is representative of a line through the origin
in affine $n+1$ dimensional space, we will have to remember that the lines
we see are points, and that projective space does not include a $(0:0:0)$
point.


% insert two side by side plots of E(A^3(RR)) and E(A^2(RR))

% I should make this into the third definition of an elliptic curve
% and I should make the elliptic curve as a torus more clear.
% We have constructed an interesting class of objects, but we have
% do not have very much context surrounding them.

% One of the most useful
% concepts for ordering and categorizing curves is the genus of a curve.
% We will not define this very carefully, but we will
% % IS THIS RIGHT???????
% state that it corresponds to the number of holes in
% the solution space of a curve over the
% algebraic closure of the field in which the curve is defined.
% The plane conics are some genus zero curves, while elliptic curves
% are genus one objects, and beyond them there exist
% hyperelliptic curves and others that have higher genus.
% We will see later through the Weierstrass $\wp$ function
% an example of this when we see that an elliptic curve
% over $\mathbb C$ has the structure of a complex torus.
% \begin{wrapfigure}{r}{0.25\textwidth}
%    \centering
%    \includegraphics[width=0.25\textwidth]{ch2-projective-curves/affine-curve}
%    \caption{}
%    $y^2 = x^3 + x + 1$
%    \label{fig:ch2-affine-11a}
% \end{wrapfigure}


These definitions of an elliptic curve are somewhat loose and allow for
many elliptic curves which are simply rotations, translations, stretches, or
some combination thereof of one another. To wrangle in some of the different
possible representations of elliptic curves, we introduce the standardization
known as the Weierstrass form.

Any nonsingular cubic projective curve can undergo a change of
variables that makes it into a curve in the \textbf{Weierstrass form}
\[ E : y^2 z + a_1 x y z + a_3 y z^2 = x^3 + a_2 x^2 z + a_4 xz^2 + a_6 z^3.\]

When the base field of the elliptic curve is not one of characteristic 2 or 3,
that is $1\cdot2\neq0$ or $1\cdot3 \neq 0$, we can make a substitution of
variables to the simpler form:
\[ E : y^2 z = x^3 + a xz^2 + bz^3.\]
The transformation to Weierstrass form standardizes which variables
of $x,y,$ and $z$ appear with exponent 3, with and without coefficients, and
ensures that the point at infinity $\mathcal O$ can be chosen as $(0 : 1 : 0)$.

Using the Weierstrass form we can finally introduce the
\texttt{EllipticCurve} class in Sage.

In Sage, an elliptic curve is always defined in Weierstrass form. Here are
four ways to define an EllipticCurve through its Weierstrass coefficients.

\begin{itemize}
\item \texttt{EllipticCurve([a1,a2,a3,a4,a6])} constructs the affine
curve given by
\[ E : y^2 + a_1 x y + a_3 y = x^3 + a_2 x^2 + a_4 x + a_6.\]
\item \texttt{EllipticCurve([a4,a6])} uses the same Weierstrass form, but $a_1=a_2=a_3=0$.
\item \texttt{EllipticCurve(R, [a1,a2,a3,a4,a6])} allows the user to construct
an elliptic curve over $R$ an arbitrary commutative ring
\item \texttt{EllipticCurve(R, [a4,a6])} combines the previous two constructions.
\end{itemize}

\begin{sage}
    EllipticCurve([1,1,0,-3,0])
''' Elliptic Curve defined by y^2 + x*y = x^3 + x^2 - 3*x over Rational Field
'''
    EllipticCurve([-1,0])
''' Elliptic Curve defined by y^2 = x^3 - x over Rational Field
'''
    EllipticCurve(FiniteField(nth_prime(10)), [-2,0,1,0,1])
''' Elliptic Curve defined by y^2 + 27*x*y + y = x^3 + 1 over Finite Field of size 29
'''
    EllipticCurve(FiniteField(5), [3,1])
''' Elliptic Curve defined by y^2 = x^3 + 2*x + 1 over Finite Field of size 5
'''
#   We can also easily retrieve the $a$-invariants used to construct a curve:
    E = EllipticCurve([1,1,0,-3,0])
    E.a_invariants()
''' (1, 1, 0, -3, 0)
\end{sage}

Having a curve in Weierstrass form also allows us to quickly determine
whether or not a curve is singular. The \textbf{discriminant of a curve $f$}
is a polynomial function $\Delta_f$ of the coefficients of $f$ such that
$\Delta _f$ is zero if and only if $f$ has roots of multiplicity greater
than one. In the case of a quadratic equation $f(x) = ax^2 + bx + c$ the
discriminant is $\Delta_f : b^2 - 4ac$, which is zero only when the quadratic
equation $f$ has a double root. If $E$ is an elliptic curve in Weierstrass
form, it is quite straightforward to calculate the discriminant $\Delta_E$.

First, we will alter the curve to another form of elliptic curves that Weierstrass worked with.
In the case where the elliptic curve's base field does not have characteristic 2 or 3,
the elliptic curve can be transformed into the form
\[ E' : y^2 = 4x^3 + b_2 x^2 + b_4 x + b_6, \]
where
$$b_2 = a_1^2 + 4a_2, \hspace{.25in} b_4 = 2a^4 + a_1a_3,
\hspace{.25in} b_6 = a_3^2 + 4 a_6, \text{ and} $$
$$b_8 = a_1^2 a_6 + 4a_2 a_6 - a_1a_3a_4 + a_2 a_3^2 - a_4^2. $$
Using these constants of $E$, we define the \textbf{discriminant}:
\[ \Delta_E = -b_2^2/b_8 -8b_4^3 - 27b_6^2 + 9b_2b_4b_6. \]

\begin{sage}
#   After defining E as an EllipticCurve, we can use the discriminant method of E.
    E = EllipticCurve([1,5])
    E.discriminant()
''' -10864
'''
#   We can also take a look at the $b$-invariants used to compute $\Delta_E$
    E.b_invariants()
''' (0, 2, 20, -1)
\end{sage}

If we try to construct a singular EllipticCurve, we get the following error.
\begin{sage}
    E = EllipticCurve([0,0])
''' ---------------------------------------------------------------------------
    ----> 1 E = EllipticCurve([Integer(0),Integer(0)])
        ...
    ArithmeticError: invariants (0, 0, 0, 0, 0) define a singular curve
\end{sage}
\bigskip

The discriminant is certainly important, because it tells us when a curve is singular,
but often we are interested in when two curves are in some sense ``the same."
We can define a quantity called the \textbf{$j$-invariant} of $E$ which only
depends on the isomorphism class of $E$ in the algebraic closure of its base field.
$$ c_4 = b_2^2 - 24b_4, \hspace{.25in} j = c_4^3 / \Delta_E. $$
That is to say, two elliptic curves have the same $j$-invariant if and only if they
are isomorphic as two curves in $k^{\text{al}}$.

% This isomorphism defines a map between the
% points of these two curves with finite kernel.
% These maps are called isogenies between elliptic curves.

\begin{sage}
#   We can calculate the $j$-invariant of a declared curve.
    E = EllipticCurve([-1,0])
    E.j_invariant()
''' 1728
'''
#   We can also define an elliptic curve by choosing one from the set of curves with
#   that $j$-invariant.
    EllipticCurve_from_j(j=1728)
''' Elliptic Curve defined by y^2 = x^3 - x over Rational Field
\end{sage}

\bigskip

% \begin{figure}[H]
% \centering
% \includegraphics[width=0.35\textwidth]{ch2-lmfdb}
% \caption{\href{http://www.lmfdb.org}{http://lmfdb.org}}
%
% \end{figure}

\begin{wrapfigure}{l}{0.35\textwidth}
   \centering
   \includegraphics[width=0.35\textwidth]{ch2-lmfdb}
   \caption{\href{http://www.lmfdb.org}{http://lmfdb.org}}
\end{wrapfigure}


The goal of this first section has been to introduce some intuitive ways of constructing
an elliptic curve. We have covered constructing a curve through its $a$-coefficients
over arbitrary fields, and through the $j$-invariant. Another very easy way to
construct an elliptic curve in Sage is by referencing its label in a popular database,
like the \href{http://www.lmfdb.org/}{L-functions and Modular Forms Database (LMFDB)}
or \href{https://johncremona.github.io/ecdata/}{John Cremona's Elliptic Curve Data}.

The \href{http://www.lmfdb.org/knowledge/show/ec.q.lmfdb_label}{LMFDB labels}
and \href{http://www.lmfdb.org/knowledge/show/ec.q.cremona_label}{Cremona's database labels}
are very similar, and both express information about
the conductor and isogeny class of a given elliptic curve, but the LMFDB labels
include an additional component called the ``isomorphism class index" that Cremona's
labels do not. To make the distinction bewteen the labels more clear, an LMFDB
label always has a ``." period or periods contained in it, while Cremona's labels do not.

\begin{sage}
    # Using Cremona's label
    E = EllipticCurve('11a')
    print E
''' Elliptic Curve defined by y^2 + y = x^3 - x^2 - 10*x - 20 over Rational Field
'''
    # Using LMFDB's label
    E = EllipticCurve('11.a')
    print E
''' Elliptic Curve defined by y^2 + y = x^3 - x^2 - 10*x - 20 over Rational Field
\end{sage}

The LMFDB allows you to \href{http://www.lmfdb.org/EllipticCurve/Q/}{browse elliptic curves
over $\mathbb Q$}, as well as explore many other parts of related number theory. It
is very useful to be able to reference these databases to find an elliptic curve
with one's desired properties.
