\begin{Verbatim}[commandchars=\\\{\},codes={\catcode`\$=3\catcode`\^=7\catcode`\_=8}]
\PYGdefault{n}{E} \PYGdefault{o}{=} \PYGdefault{n}{EllipticCurve}\PYGdefault{p}{(}\PYGdefault{l+s+s1}{\PYGdefaultZsq{}37a\PYGdefaultZsq{}}\PYGdefault{p}{)}
\PYGdefault{n}{generators} \PYGdefault{o}{=} \PYGdefault{n}{E}\PYGdefault{o}{.}\PYGdefault{n}{gens}\PYGdefault{p}{()}
\PYGdefault{n}{data\PYGdefaultZus{}pairs} \PYGdefault{o}{=} \PYGdefault{p}{[]}
\PYGdefault{n}{numerator} \PYGdefault{o}{=} \PYGdefault{l+m+mi}{0}
\PYGdefault{n}{denominator} \PYGdefault{o}{=} \PYGdefault{l+m+mi}{0}
\PYGdefault{k}{for} \PYGdefault{n}{i} \PYGdefault{o+ow}{in} \PYGdefault{n+nb}{xrange}\PYGdefault{p}{(}\PYGdefault{l+m+mi}{1}\PYGdefault{p}{,}\PYGdefault{l+m+mi}{100}\PYGdefault{p}{):}
    \PYGdefault{k}{try}\PYGdefault{p}{:}
        \PYGdefault{n}{p} \PYGdefault{o}{=} \PYGdefault{n}{nth\PYGdefaultZus{}prime}\PYGdefault{p}{(}\PYGdefault{n}{i}\PYGdefault{p}{)}

        \PYGdefault{c+c1}{\PYGdefaultZsh{} Reduce our curve, find its order, and abstract it to a generic group.}
        \PYGdefault{n}{Ep} \PYGdefault{o}{=} \PYGdefault{n}{E}\PYGdefault{o}{.}\PYGdefault{n}{reduction}\PYGdefault{p}{(}\PYGdefault{n}{p}\PYGdefault{p}{)}
        \PYGdefault{n}{Ep\PYGdefaultZus{}order} \PYGdefault{o}{=} \PYGdefault{n}{Ep}\PYGdefault{o}{.}\PYGdefault{n}{order}\PYGdefault{p}{()}
        \PYGdefault{n}{Gp} \PYGdefault{o}{=} \PYGdefault{n}{Ep}\PYGdefault{o}{.}\PYGdefault{n}{abelian\PYGdefaultZus{}group}\PYGdefault{p}{()}
        \PYGdefault{n}{Gp\PYGdefaultZus{}gens} \PYGdefault{o}{=} \PYGdefault{n}{Gp}\PYGdefault{o}{.}\PYGdefault{n}{gens}\PYGdefault{p}{()}

        \PYGdefault{c+c1}{\PYGdefaultZsh{} If the curve is singular, E.reduction(p) will have failed,}
        \PYGdefault{c+c1}{\PYGdefaultZsh{} and otherwise we compute the reduced generators as linear}
        \PYGdefault{c+c1}{\PYGdefaultZsh{} combinations of the generators of our group.}
        \PYGdefault{n}{reduced\PYGdefaultZus{}gen\PYGdefaultZus{}linear\PYGdefaultZus{}coefficients} \PYGdefault{o}{=} \PYGdefault{p}{[}\PYGdefault{n}{Gp}\PYGdefault{o}{.}\PYGdefault{n}{coordinate\PYGdefaultZus{}vector}\PYGdefault{p}{(}\PYGdefault{n}{Ep}\PYGdefault{p}{(}\PYGdefault{n}{generators}\PYGdefault{p}{[}\PYGdefault{n}{j}\PYGdefault{p}{]))}
        \PYGdefault{k}{for} \PYGdefault{n}{j} \PYGdefault{o+ow}{in} \PYGdefault{n+nb}{xrange}\PYGdefault{p}{(}\PYGdefault{n+nb}{len}\PYGdefault{p}{(}\PYGdefault{n}{generators}\PYGdefault{p}{))]}
        \PYGdefault{n}{reduced\PYGdefaultZus{}generators} \PYGdefault{o}{=} \PYGdefault{p}{[}\PYGdefault{n+nb}{sum}\PYGdefault{p}{([}\PYGdefault{n}{Gp\PYGdefaultZus{}gens}\PYGdefault{p}{[}\PYGdefault{n}{j}\PYGdefault{p}{]}\PYGdefault{o}{*}\PYGdefault{n}{reduced\PYGdefaultZus{}gen\PYGdefaultZus{}linear\PYGdefaultZus{}coefficients}\PYGdefault{p}{[}\PYGdefault{n}{k}\PYGdefault{p}{][}\PYGdefault{n}{j}\PYGdefault{p}{]}
        \PYGdefault{k}{for} \PYGdefault{n}{j} \PYGdefault{o+ow}{in} \PYGdefault{n+nb}{xrange}\PYGdefault{p}{(}\PYGdefault{n+nb}{len}\PYGdefault{p}{(}\PYGdefault{n}{Gp\PYGdefaultZus{}gens}\PYGdefault{p}{))])}
        \PYGdefault{k}{for} \PYGdefault{n}{k} \PYGdefault{o+ow}{in} \PYGdefault{n+nb}{xrange}\PYGdefault{p}{(}\PYGdefault{n+nb}{len}\PYGdefault{p}{(}\PYGdefault{n}{reduced\PYGdefaultZus{}gen\PYGdefaultZus{}linear\PYGdefaultZus{}coefficients}\PYGdefault{p}{))]}

        \PYGdefault{c+c1}{\PYGdefaultZsh{} Finally, we compute the subgroup and compare orders}
        \PYGdefault{n}{Gp\PYGdefaultZus{}subgroup} \PYGdefault{o}{=} \PYGdefault{n}{Gp}\PYGdefault{o}{.}\PYGdefault{n}{submodule}\PYGdefault{p}{(}\PYGdefault{n}{reduced\PYGdefaultZus{}generators}\PYGdefault{p}{)}
        \PYGdefault{n}{numerator} \PYGdefault{o}{+=} \PYGdefault{n}{Ep\PYGdefaultZus{}order}
        \PYGdefault{n}{denominator} \PYGdefault{o}{+=} \PYGdefault{n}{Gp\PYGdefaultZus{}subgroup}\PYGdefault{o}{.}\PYGdefault{n}{order}\PYGdefault{p}{()}
        \PYGdefault{n}{data\PYGdefaultZus{}pairs}\PYGdefault{o}{.}\PYGdefault{n}{append}\PYGdefault{p}{([}\PYGdefault{n}{p}\PYGdefault{p}{,} \PYGdefault{n}{numerator} \PYGdefault{o}{/} \PYGdefault{n}{denominator}\PYGdefault{p}{])}

    \PYGdefault{k}{except} \PYGdefault{n+ne}{AttributeError}\PYGdefault{p}{:}

        \PYGdefault{c+c1}{\PYGdefaultZsh{} It\PYGdefaultZsq{}s ok to skip singular curves}
        \PYGdefault{k}{pass}
\PYGdefault{n}{points}\PYGdefault{p}{(}\PYGdefault{n}{data\PYGdefaultZus{}pairs}\PYGdefault{p}{,} \PYGdefault{n}{figsize}\PYGdefault{o}{=}\PYGdefault{l+m+mi}{10}\PYGdefault{p}{)}
\end{Verbatim}
