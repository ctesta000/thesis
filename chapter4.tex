\chapter{A Numerical Investigation}

% Perhaps there's a story to be told here...
% What we want to understand is the relationship between
% the rational points and the finite fields' points.

% What did we see? Crazy harmonic looking wave thingies....
%

% \textbf{Topic Sentence:} Given the numerous impactful results that
% investigation into the behavior of elliptic curves have yielded,
% one might conjecture that with a small amount of computational power
% one can construct data both fascinating to explore and inspiring to analyze
% within the context of modern theory.

All mathematicians must compute data.
By computing data we verify hypotheses, test conjectures, and
observe new phenomena. Computation is an integral part of science, and
for a mathematician in the 21st century there are few better
tools available in the aid of computation than the modern computer. Through use
of programming languages to construct, test, and analyze entire
experiments, and supercomputers on which to deploy these experiments,
the scale at which we may view the phenomena
in the world of numbers is made orders of magnitude larger. In the next
chapter, I would like to invite the reader on a journey through
data. In particular, I am interested in understanding the relationship
between the infinite rational points of elliptic curves and their
reduction into finite field elliptic curves.

\section{Reducing $E(\Q)$ into $E(\F_p)$}
Let $E / \mathbb Q$ be an elliptic curve with positive rank $r$, such that
the free part of $E$ is isomorphic to $\mathbb Z^r$. Each of the infinitely
many points is a rational point, each of which we could denote
$(m_1/n_1, m_2/n_2)$. Whenever $n_1$ and $n_2$ are not divisible by $p$
for some prime $p$, then we may construct $n_1^{-1}$ and $n_2^{-1}$
within $\mathbb F_p$. Having done so, we may then replace
division with multiplication by the inverse such that
$(m_1/n_1, m_2/n_2)$ reduces into $\mathbb F_p \times \mathbb F_p$
as $(m_1n_1^{-1}, m_1n_1^{-1})$. It is from this sense in which
we might describe $E(\mathbb Q)$ reduced
into $E(\mathbb F_p)$, denoted here $E(\mathbb Q) \hookrightarrow E(\mathbb F_p)$.
Computationally, we will take specifically the free-generators for
$E(\mathbb Q)$, reduce them individually into $E(\mathbb F_p)$, and see
what subgroup they construct. When $E$ has positive rank this is a reduction
of an infinite group into a finite one.

While this behavior might be highly erratic varying from prime to prime,
it is standard in number theory into look to the limiting behavior of a distribution
when the original data is seemingly meaningless. While my
notion of interest is the ratio of the reduced points to $E(\mathbb F_p)$,
the more traditional quantity is algebraic index where $[E(\mathbb F_p) : S] =
|E(\mathbb F_p)| / |S|$ for any subgroup $S$ of $E(\mathbb F_p)$.
Consider the following function, and what it might do in the limit.
\[ F_E(p) = \frac{\sum_{\text{primes }p} |E(\mathbb F_p)|}{
\sum_{\text{primes }p}|E(\mathbb Q) \text{ reduced into } E(\mathbb F_p)|}. \]

\pagebreak
\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{chapter4/index_plot_37a}
\caption{The graph of $(p, [E(\mathbb F_p) : E(\mathbb Q) \hookrightarrow E(\mathbb F_p)])$ for 37a} a rank
one curve given by $y^2 + y = x^3 - x$.
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{chapter4/accumulation_index_plot_37a}
\caption{The graph of $(p, F_E(p))$ for {\scriptsize \texttt{E = EllipticCurve('37a')}}.}
\label{accumulation_plots}
\end{figure}

\pagebreak

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{chapter4/accumulation_index_plot_30_curves}
\caption{Graphs of $(p, F_E(p))$ for 50 curves, 10 of each rank}
\end{figure}
\begin{figure}[H]
\centering
\begin{subfigure}{.3\textwidth}
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_plot_10_rank_1}
\caption*{rank 1}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_plot_10_rank_2}
\caption*{rank 2}
\end{subfigure}
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_plot_10_rank_3}
\caption*{rank 3}
\end{subfigure}
\vskip\baselineskip
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_plot_10_rank_4}
\caption*{rank 4}
\end{subfigure}
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_plot_10_rank_5}
\caption*{rank 5}
\end{subfigure}
\end{figure}

Blue denotes rank 1, purple rank 2, orange rank 3, green
rank 4, and red rank 5. While I am not surprised to see that when there are
more generators that the subgroup becomes larger, I am surprised that
even for rank 4 and rank 5 the data does not appear convergent. Rather,
as the prime becomes larger we see the ratio shift away from one in
favor of jumping up in quantity, where there must have been at least one point
in $E(\F_p)$ not in the
reduction of $E(\mathbb Q) \hookrightarrow E(\mathbb F_p)$ at certain primes. While
rank 2, 3, 4, and 5 appear to each occupy a fairly small range of values,
within these ranges it does not appear that one quantity is strongly preferred over
others. It should be noted that the individual plots are each on their own scale, such
that rank 3, 4, and 5 have magnified $y$-axes to see the detail of their behavior.
It certainly seems difficult to reason from these plot if this ratio converges
for any elliptic curves in the limit.

It appears that generally $E(\mathbb Q) \hookrightarrow
E(\mathbb F_p)$ often has index one until a certain
prime where it skips. The horizontal lines at one indicate that
the order of the subgroup of $E(\F_p)$ generated by
the free generators of $E(\Q)$ is and has been equal to $|E(\F_p)|$ for
primes less than or equal to the $n^\text{th}$ prime. Then, at a
particular prime the free-generators fail to generate the whole finite
field elliptic curve, so the accumulation function skips upward. However,
in large rank curves it seems that $E(\Q) \hookrightarrow E(\F_p)$
and $E(\F_p)$ are equal often, and thus we see a steady decay acting
on each of the curves' accumulation functions after such a skip.

So, I did the sane thing to do, and I computed the largest data set I could in a few
hours on my computer. Additionally, I inverted the ratio back to my original
notion of the ratio of the number of reduced points from $E(\mathbb Q)$ to the
number of points in $E(\mathbb F_p)$ so that the graphs wouldn't be unbounded.
In many of the following graphs, the horizontal axis denotes the $n^\text{th}$
prime $p$.


\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{chapter4/accumulation_index_huge}
\caption{The graph of $(n, \frac{1}{F_E(p)})$.}
\label{accumulation_index_huge}
\end{figure}
\begin{figure}[H]
\centering
\begin{subfigure}{.3\textwidth}
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_1}
\caption*{rank 1}
\end{subfigure}%
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_2}
\caption*{rank 2}
\end{subfigure}
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_3}
\caption*{rank 3}
\end{subfigure}
\vskip\baselineskip
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_4}
\caption*{rank 4}
\end{subfigure}
\begin{subfigure}{.3\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/accumulation_index_5}
\caption*{rank 5}
\end{subfigure}
\caption{Accumulation Function $\frac{1}{F_E(p)}$ separated for each rank}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
\caption*{rank 1}
\includegraphics[width=1\textwidth]{chapter4/example_accumulation_1}
\centering
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 2}
\includegraphics[width=1\textwidth]{chapter4/example_accumulation_2}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 3}
\includegraphics[width=1\textwidth]{chapter4/example_accumulation_3}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 4}
\includegraphics[width=1\textwidth]{chapter4/example_accumulation_4}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 5}
\includegraphics[width=1\textwidth]{chapter4/example_accumulation_5}
\end{subfigure}
\caption{Examples of $\frac{1}{F_E(p)}$ for one curve of each rank 1-5}
\label{example_accumulation}
\end{figure}

I have used 315 curves to make these plots, 100 of ranks 1, 2, 3, and then I used all the
rank 4 and rank 5 curves presently in the Sage
\href{http://doc.sagemath.org/html/en/reference/curves/sage/schemes/elliptic_curves/ec_database.html}{sage.schemes.elliptic\_curves.ec\_database}.

\begin{figure}[H]
\centering
\includegraphics[width=.7\textwidth]{chapter4/accumulation_hist_huge}
\caption{And then I histogrammed.}
\label{accumulation_hist_huge}
\end{figure}

Perhaps it is because there are simply more generators
for larger rank curves that the value of peaks descend
in order of rank 5 to rank 1. However, besides this simple
argument, I can do little to explain this data. Why is rank 1
trimodal? Why are there so many peaks for ranks 1 and 2?
What governs the decay of these distributions? I do not know.

% I decided to quantify the most tangible feeling feature of this data,
% and asked Sage which divisor of $|E(\F_p)|$ was $|E(\Q) \hookrightarrow E(\F_p)|$
% for my sampled curves. By this, I meant something specific. When
% given the list of divisors of $|E(\F_p)|$ in order from 1 up to $|E(\F_p)|$,
% then $|E(\Q)|$ must be in this list, and it must have an index we might call $n$.
% Dividing $n$ by the number of divisors in this list for each curve, we have a measure for
% how large the subgroup is relative to how large it could have been observed through
% the divisors of $|E(\F_p)|$.

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
\caption*{rank 1}
\includegraphics[width=1\textwidth]{chapter4/EFp_over_EQ_rank_1}
\centering
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 2}
\includegraphics[width=1\textwidth]{chapter4/EFp_over_EQ_rank_2}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 3}
\includegraphics[width=1\textwidth]{chapter4/EFp_over_EQ_rank_3}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 4}
\includegraphics[width=1\textwidth]{chapter4/EFp_over_EQ_rank_4}
\end{subfigure}
\begin{subfigure}{.33\textwidth}
\centering
\caption*{rank 5}
\includegraphics[width=1\textwidth]{chapter4/EFp_over_EQ_rank_5}
\end{subfigure}
\caption{$|E(\F_p)|/|E(\Q) \hookrightarrow E(\F_p)|$ at the $n^\text{th}$ prime}
\label{which_divisor}
\end{figure}

Not making significant progress in understanding the
distribution of the accumulation function, I was driven to find
simpler patterns. In the last plots I believe I have been successful.
Rank 5 in figure \ref{which_divisor}
characterizes the skipping behavior
I had noticed previously, and there are some interesting lower bounds on these
graphs for rank 4 and 5. I do not have any explanation for why
rank four $E(\Q) \hookrightarrow E(\F_p)$ subgroups appear in my data as
a third of the whole group or greater, or similarly why the rank 5 subgroups appear
only as half or the whole. In effort to understand this and find
motivation for it, I plotted $|E(\Q) \hookrightarrow E(\F_p)|$ for a single curve.

\begin{figure}[H]
\centering
\includegraphics[width=.6\textwidth]{chapter4/37a_EQ}
\caption{$|E(\Q) \hookrightarrow E(F_p)|$ for 37a}
\end{figure}

The thickest large blue band is visually almost exactly the Hasse bound plot
in figure \autoref{hasse_bound}, and indeed corresponds with when the
point $(0,-1)$ on $y^2 + y = x^3 - x$ generates all of $E(\F_p)$. This is
plot is certainly visually striking, but I believe it only tells me what I
already knew: the ratio of rank one curves' reduced-$E(\Q)$ groups' size
to the corresponding $|E(\F_p)|$ value is highly scattered and difficult
to predict.

These plots are certainly not completely random, and rank 5 in figure \ref{which_divisor}
exemplifies the ``skipping" behavior I have described. However, more
than anything else I have simply confirmed for myself that understanding
the rank of elliptic curves is truly subtle and hard. At this point,
I knew I needed simpler questions if I wanted simpler
answers.

There is one feature which particularly stands out
to me in my data that I ask the reader not to forget--the horizontal lines at one present in
figures \ref{accumulation_plots}, \ref{accumulation_index_huge}, and \ref{which_divisor}.
This is when reduction of $E(\Q)$ is onto $E(\F_p)$.

\section{Generating a Subgroup with $(0,0)$}
Rank is hard to understand and use experimentally, so we will pivot away
from using it in favor of experiments with less choice
involved for the experimenter (such as the choice of sampling).
In an effort to replace the original problem, I now want to measure $\langle P \rangle
\subseteq E(\F_p)$, where $P$ is some point of interest and easier to find than
the generators of $E(\Q)$. In response to this, my advisors suggested using the following family with which we can new define functions at each prime measuring
more refined accumulated quantities.
\[ \{ E_{A,B} : y^2 + y = x^3 + Ax^2 + Bx \text{ for } A,B \in \F_p \text{ and }\Delta(E_{A,B}) \neq 0 \} , \]
\[ \Delta(E_{A,B}) = 16 A^{2} B^{2} - 16 A^{3} - 64 B^{3} + 72 A B - 27. \]
Notice conveniently, the reduction of $E_{A,B}$ into $\F_p$ always
contains the point $(0,0)$. In particular, we will define
the following functions at each prime number.
\[ F_1(p) = \sum_{A,B \Mod p} |E_{A,B} (\mathbb F_p)|, \]
\[ F_2(p) = \sum_{A,B \Mod p} |\langle (0,0) \in E_{A,B}(\mathbb F_p) \rangle |,\]
\[ F_3(p) = \sum_{A,B \Mod p} [E_{A,B}(\mathbb F_p) : \langle (0,0) \rangle ],\]
\[ F_4(p) = \sum_{A,B \Mod p} 1/[E_{A,B}(\mathbb F_p) : \langle (0,0) \rangle ].\]
Why is this a suitable refinement of our original question about reducing $E(\Q)$? Because
often enough $(0,0)$ is one of the free generators of the rational points on an elliptic curve
parametrized by $y^2 + y = x^3 + Ax^2 + Bx$. This can be verified for any prime
(and in particular $p=17$) through the following code snippet.
\begin{multicols}{2}
\begin{sage}
    from itertools import product
    for (A,B) in product(range(17), range(17)):
        try:
            E = EllipticCurve([0,A,1,B,0])
            print E.gens()
        except:
            pass
''' []
    [(0 : 0 : 1)]
    [(2 : 3 : 1)]
    [(0 : 0 : 1)]
    [(0 : 0 : 1), (1/4 : 5/8 : 1)]
    [(0 : 0 : 1), (1 : 2 : 1)]
    ...
\end{sage}
\end{multicols}
Since there are approximately $p+1$ points
on any $E_{A,B}(\F_p)$, and there are $p$ choices for each of $A$ and $B$, we should
expect that $F_1$ will grow cubically with $p$. $F_2$ is bounded above by
$F_1$, and since $|\langle (0,0) \rangle | \geq 1$, we can expect it to
grow at least as fast as quadratically with respect to $p$. $F_3$ and $F_4$
are more subtle, although intimately related to each other as accumulation functions
of each others' inversed summand.

\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{chapter4/f_family}
\caption{$F_1, F_2, F_3, F_4$ plotted as functions of $n^\text{th}$ primes on a loglog plot}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.4\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/f1_div_p3}
\caption*{$F_1(p)/p^3$}

\end{subfigure}
\begin{subfigure}{.4\textwidth}
\centering
\includegraphics[width=1\textwidth]{chapter4/f2_over_p3}
\caption*{$F_2(p)/p^3$}

\end{subfigure}
\begin{subfigure}{.4\textwidth}
\includegraphics[width=1\textwidth]{chapter4/f2_over_f1}
\end{subfigure}
\caption*{$F_2(p)/F_1(p)$}
\caption{Analysis of $F_1$ and $F_2$ for 100 Primes}
\label{analyze_f1_f2}
\end{figure}

With figure \ref{analyze_f1_f2}, we find that $F_1(p) \sim p^3$, $F_2(p) \sim 0.61 p^3$,
and $\frac{F_2(p)}{F_1(p)} \sim 0.61$. Continuing on, we will take a look
at $F_3$, and skip over $F_4$ since its information is nearly equivalent.

\begin{figure}[H]
\centering
\begin{subfigure}{.45\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f3}
\caption{$F_3(p)$}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f3_over_p2}
\caption{$F_3(p)/p^2$}
\end{subfigure}
\vskip\baselineskip
\begin{subfigure}{.45\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f3_div_p2_semilog}
\caption{$F_3(p)/p^2$ on semilogx scale}
\end{subfigure}
\begin{subfigure}{.45\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f3_over_p2lnp}
\caption{$\frac{F_3(p)}{p^2\ln p}$}
\end{subfigure}
\caption{Analyzing $F_3$ for 100 Primes}
\end{figure}

We expect that $F_3$ cannot possibly grow as fast as $p^3$, since
the generated subgroup is often not the whole group. However,
as we reasoned before, it should be faster than $p^2$, since
$|\langle (0,0) \rangle|$ is always at least one, but usually larger.
In the above plots, we normalize $F_3$ by $p^2$, and find that the
remaining normalized plot is straight on a semilogx plot. In Sage,
whenever constructing a scatter plot with {\scriptsize \texttt{points}},
it's easy to use the parameters
{\scriptsize \texttt{scale='semilogx'}, \texttt{scale='semilogy'},
\texttt{scale='loglog'}} to check out if data grows
exponentially, logarithmically, or according to power laws. After
determining the data grows logarithmically, we normalize by a logarithm
and find that in the last plot $\frac{F_3(p)}{p^2\ln p} \sim .82$.

While this data is encouraging in that it is clearly convergent,
these ratios do not immediately yield the truth.
61\% and 82\% are just approximations of the limits I believe exist,
However, these limits' computation is, for the moment, inaccessible to me.

Rather, what I have found instead of a limit value is an incredible
apparent bound above the ratio $F_1(p)/F_2(p)$.

\begin{figure}[H]
\centering
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f1_div_f2}
\caption{$F_1(p)/F_2(p)$ is Less Than $\sqrt{e}$ for 100 Primes}
\end{subfigure}
\vskip\baselineskip
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[width=.8\textwidth]{chapter4/f1_over_f2_logarithm}
\caption{$\ln(F_1(p)/F_2(p))$ is Less Than 1/2 for 100 Primes}
\end{subfigure}
\caption{$F_1$ and $F_2$ Revisited with Logarithms}
\end{figure}

I originally plotted $F_2(p) / F_1(p)$ because I reasoned that
as a ratio of the group, the order of $\langle (0,0) \rangle$
might have regular behavior. However, $F_1(p) / F_2(p)$ resembles
the algebraic index much more, and has been highly worth investigating.
Noticing that it is bounded by $\sqrt{e}$ has startled me incredibly.
After seeing this, I found I was even better able to capture this bound
with a plot of $(F_1(p),F_2(p))$. The next plot is bounded and hugged so nicely
by the plot of $y = \frac{x}{\sqrt{e}}$ out to $10^8$, although the
proximity to the line seems to be decaying.

\begin{figure}[H]
  \centering
\includegraphics[width=.8\textwidth]{chapter4/f1_f2_plot_bounded_by_sqrt_e}
\vskip\baselineskip
\includegraphics[width=.8\textwidth]{chapter4/sqrt_e_error}
\caption{Graphs of ($F_1$(p), $F_2(p)$) and ($p$, $F_2(p)\sqrt{e}-F_1(p)$)}
\label{sqrt_e}
\end{figure}

Why any of this happens, I do not know. It seems perfectly
reasonable to me that the data could tend to a number less
than or near $\sqrt e$ in the limit. However, it has surprised
me to find such a strong apparent bound at all in the first
100 values of the ratio of $F_1 / F_2$.

\section{When is the Subgroup the Group?}
\begin{figure}[H]
  \centering
  \includegraphics[width=.6\textwidth]{chapter4/EFp_S_points}
  \caption{A Graph of $(|E_{A,B}(\F_p)|,\, |\langle (0,0) \rangle|)$ for 100 Primes}
  \label{EFp_S}
\end{figure}
While the previous experiments have confounded, confused, and bewildered me
at times, I have found a way forward. In figure \ref{EFp_S}, there is a
ray extending from the origin at slope $1$, $\frac{1}{2}$, $\frac{1}{3}$, $\frac{1}{4}$,
and so on. Each of the rays extending from the origin makes up some
proportion of the graph. Not only is it feasible to measure this
proportion, but in some cases we may easily uncover the limiting behavior of the
proportion. Now is when I would
like the reader to recollect the horizontal lines at one from \textit{Reducing $E(\Q)$
into $E(\F_p)$}, because the first natural question in this new investigation
is how often is $\langle (0,0) \rangle = E(\F_p)$. This frequency is exactly
the proportion of the graph at each prime in figure \ref{EFp_S} which has slope one.

To make my question more precise, I will investigate the following function
to the best of my ability. \[ \tilde F(p,n) = \frac{\# \{ A,B \Mod p \text{ such that } n \text{ divides } [ E_{A,B} \colon \langle (0,0) \rangle] \} }{
\# \{ \text{nonsingular } E_{A,B} \Mod p \}} \]
We will consider separately how often $\langle (0,0) \rangle = E_{A,B}(\F_p)$,
and for all values of $n > 1$ the function
$\tilde F(p,n)$ measures for each prime family the proportion of curves
which would appear in figure \ref{EFp_S} on the rays with inverse slope
divisible by $n$.

\begin{figure}[H]
\centering
\includegraphics[width=.5\textwidth]{chapter4/surjectivity_probability}
\caption{How Often is $\langle (0,0) \rangle = E_{A,B}(\F_p)$?}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.5\textwidth]{chapter4/divisibility_two}
\caption{$\tilde F(p,2)$}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=.5\textwidth]{chapter4/divisibility_three}
\caption{$\tilde F(p,3)$}
\end{figure}

It appears that the ratio of curves in our families for each
prime with $\langle (0,0) \rangle = E_{A,B}(\F_p)$ converges
to approximately \~$44\%$. Further, for 2-divisibility and
3-divisibility, three distinct limits have been computed for
which I have found that the data converges to according to an inverse
power law. For $\tilde F(p,3)$, I had to divide the primes into those
which are $1\Mod3$ and $2\Mod3$ to make sense of the data.
\[ \tilde F(p,2) \sim \frac{10}{24},\hspace{.25in} \tilde F(p=1 \Mod 3, 3) \sim \frac{33}{216},
\hspace{.25in} \tilde F(p=2 \Mod 3, 3) \sim \frac{36}{216}. \]

\begin{figure}[H]
\centering
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[width=.6\textwidth]{chapter4/divisibility_claim_1}
\caption*{Error $\frac{10}{24} - \tilde F(p,2)$ on a loglog plot}
\end{subfigure}
\vskip\baselineskip
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[width=.6\textwidth]{chapter4/divisibility_claim_2}
\caption*{Error $\frac{36}{216} - \tilde F(p=2 \Mod 3,3)$ on a loglog plot}
\end{subfigure}
\caption{Error Decays Exponentially}
\end{figure}

A Galois theoretical heuristic also supports these claims.
Through using the group structure of $E(\F_p)$ and the doubling formula
we may calculate how and when we should expect the $(0,0)$ point to appear in the
group's multiplication table. Through the study of the roots of particular polynomials,
perhaps we will find a method to compute the limit value of $\tilde F(p,n)$
for any $n$. However, that is for another paper.

I have yet to find a direct argument for the
ratio of each prime family of curves which have the property
that
$\langle (0,0) \rangle = E(\F_p)$. However, in answering how often
the index is two-divisible and three-divisible, one more path forward in
investigation of the
subgroups of $E(\F_p)$ generated by $E(\Q)$'s free-generators
is established. Given the values of $\tilde F(p,n)$ for all
integers $n > 1$, the ratio for which $\langle (0,0) \rangle = E(\F_p)$
becomes a simple inclusion/exclusion principle based computation.
Understanding this ratio is one
way we may soon know something more about the relationship between
the infinite free part of elliptic curves and their finite field
reductions.

\section*{Conclusion of Experiments}
What do we make of our experiments in sum? Is the failure to
understand the reduction of rational elliptic curves $E(\Q)$
into finite field elliptic curves $E(\F_p)$ equivalent to having
learned nothing? Is understanding $E(\F_p)$ all we can hope for,
and that the rank of an elliptic curve is and forever will be
beyond us? No. While elliptic curves will remain mysterious until
we can more fully understand the rank, it is through interrelating
the rank of elliptic curves to objects which we more thoroughly
understand that we will be able to uncover the explanations to
elliptic curve's rank.  In our first experiment
we have verified through computation that understanding the rank of an
elliptic curve and its influence on the curves behavior is
no small task. In the second experiment, we have demonstrated that we
can meaningfully connect the theory of the free part of
rational elliptic curves to finite field elliptic curves. Finally, in the
third experiment, we demonstrate concrete means to use this
connection to uncover the laws governing part of an elliptic curves
behavior. In sum, we have performed a small investigation into
the relationship between an elliptic curve's free part and the curves
algebraic behavior in finite field reductions.


% For others, the way
% to work with elliptic curves may be through modular forms.


\section{On Progress}
Never before have the means of scientific discovery been so accessible as
they are now in mathematics. SageMath and all other programming languages make
it such that any individual may begin on the path to discovery, and hope for
success. Having discovered the remarkable apparent $\sqrt{e}$ bound,
a few limit values for $\tilde F(p,n)$, and several beautiful plots along the way,
it is evident that progress through numerical investigation is truly viable.
Elliptic curves may remain mysterious for a long time to come, but progress
is undoubtedly certain, perhaps from amateurs and professionals alike.
